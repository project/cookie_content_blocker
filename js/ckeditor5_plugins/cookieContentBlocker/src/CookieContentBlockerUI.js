import {Plugin} from 'ckeditor5/src/core';
import {ButtonView, clickOutsideHandler, ContextualBalloon} from 'ckeditor5/src/ui';
import pluginIcon from '../icons/cookiecontentblocker.svg';
import CookieContentBlockerFormView from "./ui/CookieContentBlockerFormView";

export default class CookieContentBlockerUI extends Plugin {
  static get requires() {
    return [ContextualBalloon];
  }

  init() {
    this._createButton();
    this._createForm();
  }

  _createButton() {
    const editor = this.editor;

    editor.ui.componentFactory.add('cookieContentBlockerButton', (locale) => {
      const button = new ButtonView(locale);

      button.set({
        label: 'Cookie content blocker',
        icon: pluginIcon,
        tooltip: true
      });

      this.listenTo(button, "execute", () => {
        this._showForm();
      });

      return button;
    });
  }


  /**
   * Creates the form.
   *
   * @private
   */
  _createForm() {
    const editor = this.editor;

    // Get the Drupal Configurations.
    const options = editor.config.get(`cookieContentBlockerOptions`);
    const command = editor.commands.get('CookieContentBlockerCommand');

    /**
     * The contextual balloon plugin instance.
     */
    this._balloon = editor.plugins.get("ContextualBalloon");

    /**
     * A form containing class, title & id inputs, used to change data-attributes values.
     */
    this._form = new CookieContentBlockerFormView(editor.locale, options, command);

    // When the form is submitted, close the form instead of POST the whole page.
    this.listenTo(this._form, "save", () => {
      // Grab values from the input fields.
      const content = this._form.contentTextArea.fieldView.element.value;
      const settings = {
        button_text: this._form.buttonTextInput.fieldView.element.value,
        blocked_message: this._form.blockedMessageTextArea.fieldView.element.value,
        show_placeholder: this._form.showPlaceholderSwitch.isOn,
        show_button: this._form.showButtonSwitch.isOn,
        enable_click: this._form.enableClickSwitch.isOn,
        category: this._form.categoryDropdown.selectedCategory
      };

      editor.execute('CookieContentBlockerCommand', content, settings);
      editor.editing.view.focus();
      // Hide the form view after submit.
      this._hideUI();
    });

    // Hide the form view after clicking the "Cancel" button.
    this.listenTo(this._form, 'cancel', () => {
      this._hideUI();
    });

    // Close the form on Esc key press.
    this._form.keystrokes.set("Esc", (data, cancel) => {
      this._hideUI();
      cancel();
    });

    // Hide the form view when clicking outside the balloon.
    clickOutsideHandler({
      emitter: this._form,
      activator: () => this._isVisible,
      contextElements: [this._balloon.view.element],
      callback: () => this._hideUI()
    });
  }

  _hideUI() {
    // Clear the input field values and reset the form.
    this._form.contentTextArea.fieldView.value = '';
    this._form.buttonTextInput.fieldView.value = '';
    this._form.blockedMessageTextArea.fieldView.value = '';
    this._form.showPlaceholderSwitch.isOn = false;
    this._form.showButtonSwitch.isOn = false;
    this._form.enableClickSwitch.isOn = false;

    this._form.element.reset();

    this._balloon.remove(this._form);

    // Focus the editing view after inserting the cookie content blocker so the user can start typing the content
    // right away and keep the editor focused.
    this.editor.editing.view.focus();
  }

  /**
   * Shows the form in a balloon.
   */
  _showForm() {
    if (this._isVisible) {
      return;
    }

    const editor = this.editor;
    const command = editor.commands.get("CookieContentBlockerCommand");

    if (!this._isInBalloon) {
      // Place the form into the Balloon.
      this._balloon.add({
        view: this._form,
        position: this._getBalloonPositionData()
      });
    }

    // Fill the form using the state of the command.
    if (command.settings) {
      this._form.buttonTextInput.fieldView.value = command.settings.button_text;
      this._form.blockedMessageTextArea.fieldView.value = command.settings.blocked_message;
      this._form.showPlaceholderSwitch.isOn = command.settings.show_placeholder;
      this._form.showButtonSwitch.isOn = command.settings.show_button;
      this._form.enableClickSwitch.isOn = command.settings.enable_click;
      this._form.categoryDropdown.selectedCategory = command.settings.category;
    } else {
      this._form.buttonTextInput.fieldView.value = Drupal.t('Show content');
      this._form.blockedMessageTextArea.fieldView.value = Drupal.t('You have not yet given permission to place the required cookies. Accept the required cookies to view this content.');
      this._form.showButtonSwitch.isOn = true;
      this._form.showPlaceholderSwitch.isOn = true;
      this._form.enableClickSwitch.isOn = true;
    }

    if (command.content) {
      this._form.contentTextArea.fieldView.value = command.content;
    }
    else {
      this._form.contentTextArea.fieldView.value = '';
    }
  }

  /**
   * Returns `true` when the form is the visible view in the balloon.
   *
   * @type {Boolean}
   */
  get _isVisible() {
    return this._balloon.visibleView === this._form;
  }

  /**
   * Returns `true` when the form is in the balloon.
   *
   * @type {Boolean}
   */
  get _isInBalloon() {
    return this._balloon.hasView(this._form);
  }

  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;

    // Set a target position by converting view selection range to DOM
    target = () => view.domConverter.viewRangeToDom(viewDocument.selection.getFirstRange());

    return {
      target
    };
  }

}
