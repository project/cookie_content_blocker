import {InputView} from 'ckeditor5/src/ui';

export default class TextAreaView extends InputView {
  constructor(locale) {
    super(locale);

    // An entry point to binding observables with DOM attributes,
    // events and text nodes.
    const bind = this.bindTemplate;

    this.setTemplate({
      tag: 'textarea',
      attributes: {
        class: [
          'ck',
          'ck-input',
          'ck-textarea',
          bind.if('isFocused', 'ck-input_focused'),
          bind.if('isEmpty', 'ck-input-text_empty'),
          bind.if('hasError', 'ck-error')
        ],
        id: bind.to('id'),
        readonly: bind.to('isReadOnly'),
        inputmode: bind.to('inputMode'),
        'aria-invalid': bind.if('hasError', true),
        'aria-describedby': bind.to('ariaDescribedById')
      },
      on: {
        input: bind.to((...args) => {
          this.fire('input', ...args);
          this._updateIsEmpty();
        }),
        change: bind.to(this._updateIsEmpty.bind(this))
      }
    });
  }
}
