import {addListToDropdown, ButtonView, createDropdown, createLabeledInputText, FormHeaderView, LabeledFieldView, ViewModel, SwitchButtonView, View} from 'ckeditor5/src/ui';
import TextAreaView from './TextAreaView.js';
import {Collection, KeystrokeHandler} from "ckeditor5/src/utils";
import {icons} from "ckeditor5/src/core";

export default class CookieContentBlockerFormView extends View {
  constructor(locale, options, command) {
    super(locale);

    /**
     * A collection of child views in the form.
     *
     * @readonly
     * @type {module:ui/viewcollection~ViewCollection}
     */
    this.children = this.createCollection();

    /**
     * An instance of the {@link module:utils/keystrokehandler~KeystrokeHandler}.
     *
     * @readonly
     * @member {module:utils/keystrokehandler~KeystrokeHandler}
     */
    this.keystrokes = new KeystrokeHandler();

    this.contentTextArea = this._createTextArea('Enter the content (HTML) that should be blocked until consent is given here.');

    this.categoryDropdown = createDropdown(this.locale);
    this.categoryDropdown.buttonView.set({
      label: Drupal.t('Cookie category'),
      withText: true
    });

    this.categoryDropdown.set('selectedCategory');

    this.listenTo(this.categoryDropdown, 'execute', evt => {
      this.categoryDropdown.selectedCategory = evt.source.categoryKey;
    });

    addListToDropdown(this.categoryDropdown, getDropdownItemsDefinitions(this.categoryDropdown, options.categories));

    this.blockedMessageTextArea = this._createTextArea('Blocked message');

    this.buttonTextInput = this._createLabeledInputView('Button text', '');

    this.showButtonSwitch = this._createSwitchButtonView('Show button');
    this.showPlaceholderSwitch = this._createSwitchButtonView('Show placeholder');
    this.enableClickSwitch = this._createSwitchButtonView('Enable changing consent by clicking on the whole blocked content placeholder');

    /**
     * A button used to submit the form.
     *
     * @member {module:ui/button/buttonview~ButtonView} #saveButtonView
     */
    this.saveButtonView = this._createButton(
      "Save",
      icons.check,
      "ck-button-save",
      "save"
    );

    /**
     * A button used to cancel the form.
     *
     * @member {module:ui/button/buttonview~ButtonView} #cancelButtonView
     */
    this.cancelButtonView = this._createButton(
      "Cancel",
      icons.cancel,
      "ck-button-cancel",
      "cancel"
    );

    // Form header.
    this.children.add(
      new FormHeaderView(locale, {
        label: this.t("Cookie Content Blocker")
      })
    );

    this.children.add(this._createRowView([this.contentTextArea]));
    this.children.add(this._createRowView([this.categoryDropdown]));
    this.children.add(this._createRowView([this.blockedMessageTextArea]));
    this.children.add(this._createRowView([this.showPlaceholderSwitch]));
    this.children.add(this._createRowView([this.showButtonSwitch]));
    this.children.add(this._createRowView([this.enableClickSwitch]));
    this.children.add(this._createRowView([this.buttonTextInput]));

    this.children.add(
      this._createRowView(
        [this.saveButtonView, this.cancelButtonView],
        ["ck-table-form__action-row"]
      )
    );

    this.setTemplate({
      tag: 'form',
      attributes: {
        class: ['ck', 'ck-cookie-content-blocker-form'],
        tabindex: '-1'
      },
      children: this.children
    });
  }

  /**
   * @inheritdoc
   */
  destroy() {
    super.destroy();

    this.keystrokes.destroy();
  }

  /**
   * Creates the button view.
   *
   * @param {String} label
   *   The button label
   * @param {String} icon
   *   The button's icon.
   * @param {String} className
   *   The additional button CSS class name.
   * @param {String} [eventName]
   *   The event name that the ButtonView#execute event will be delegated to.
   * @returns {module:ui/button/buttonview~ButtonView}
   *   The button view instance.
   *
   * @private
   */
  _createButton(label, icon, className, eventName) {
    const button = new ButtonView(this.locale);

    button.set({
      label: Drupal.t(label),
      icon,
      withText: true
    });

    button.extendTemplate({
      attributes: {
        class: className
      }
    });

    if (eventName) {
      button.delegate("execute").to(this, eventName);
    }

    return button;
  }

  /**
   * Creates an input with a label.
   *
   * @returns {module:ui/labeledfield/labeledfieldview~LabeledFieldView}
   *   Labeled field view instance.
   *
   * @private
   */
  _createLabeledInputView(label, infoText) {
    const labeledInput = new LabeledFieldView(
      this.locale,
      createLabeledInputText
    );

    labeledInput.label = Drupal.t(label);
    labeledInput.infoText = infoText;

    return labeledInput;
  }

  _createTextArea(label) {
    const labeledInput = new LabeledFieldView(
      this.locale,
      createLabeledTextArea
    );

    labeledInput.label = Drupal.t(label);

    return labeledInput;
  }

  /**
   * Creates a complex Row View.
   *
   * @returns {module:ui/view~View}
   *   The Row view.
   *
   * @private
   */
  _createRowView(children, classes) {
    const view = new View();
    view.setTemplate({
      tag: "div",
      attributes: {
        class: ["ck", "ck-form__row", classes !== undefined ? classes : ""]
      },
      children
    });

    return view;
  }

  /**
   * Creates a switch button.
   *
   * @param label
   *
   * @returns {module:ui/view~SwitchButtonView}
   *
   * @private
   */
  _createSwitchButtonView(label) {
    const button = new SwitchButtonView(this.locale);

    button.set({
      label: Drupal.t(label),
      withText: true,
      isOn: false
    });

    button.on('execute', () => {
      button.isOn = !button.isOn;
    });

    return button;
  }

  render() {
    super.render();
    // Start listening for the keystrokes coming from #element.
    this.keystrokes.listenTo( this.element );
  }

}

function getDropdownItemsDefinitions(dropDownView, categories) {
  const itemDefinitions = new Collection();

  for (const key in categories) {
    const definition = {
      type: 'button',
      id: key,
      model: new ViewModel({
        categoryKey: key,
        label: categories[key],
        withText: true
      })
    };

    definition.model.bind('isOn').to(dropDownView, 'selectedCategory', (value) => {
      return value === definition.model.categoryKey;
    });

    itemDefinitions.add(definition);
  }

  if (itemDefinitions.get(0) === null) {
    const definition = {
      type: 'button',
      id: 'empty',
      isEnabled: false,
      model: new ViewModel({
        categoryKey: 'empty',
        label: 'No categories. Add categories in Cookie Content Blocker settings.',
        withText: true,
        isEnabled: false,
      })
    };

    itemDefinitions.add(definition);
  }

  return itemDefinitions;
}

export function createLabeledTextArea(labeledFieldView, viewUid, statusUid) {
  const inputView = new TextAreaView(labeledFieldView.locale);
  inputView.set({
    id: viewUid,
    ariaDescribedById: statusUid
  });
  inputView.bind('isReadOnly').to(labeledFieldView, 'isEnabled', value => !value);
  inputView.bind('hasError').to(labeledFieldView, 'errorText', value => !!value);
  inputView.on('input', () => {
    // UX: Make the error text disappear and disable the error indicator as the user
    // starts fixing the errors.
    labeledFieldView.errorText = null;
  });
  labeledFieldView.bind('isEmpty', 'isFocused', 'placeholder').to(inputView);
  return inputView;
}
