import {Command} from 'ckeditor5/src/core';

export default class CookieContentBlockerCommand extends Command {
  /**
   * Constructs a new object.
   *
   * @param {module:core/editor/editor~Editor} editor
   *   The editor instance.
   * @param {Object<string>} options
   *   All available Drupal options.
   */
  constructor(editor, options) {
    super(editor);
    this.options = options;
  }

  /**
   * @inheritdoc
   */
  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;
    const element = getSelectedCookieContentBlockerModelWidget(selection);

    this.isEnabled = true;

    if (element) {
      this.content = element.getAttribute('content') || '';
      this.settings = JSON.parse(element.getAttribute('settings'));
    } else {
      this.content = null;
      this.settings = null;
    }
  }

  /**
   * Set attributes to the element.
   */
  execute(content, settings) {
    const editor = this.editor;
    const model = editor.model;
    const selection = model.document.selection;

    model.change(writer => {
      let selectedElement = getSelectedCookieContentBlockerModelWidget(selection);

      if (!selectedElement) {
        // Create a <cookiecontentblocker> element.
        selectedElement = writer.createElement('cookiecontentblocker');
        model.insertObject(selectedElement, null, null, {setSelection: 'on'});
      }

      writer.setAttribute('content', content, selectedElement);
      writer.setAttribute('settings', JSON.stringify(settings), selectedElement);
    });
  }
}

function getSelectedCookieContentBlockerModelWidget(selection) {
  const selectedElement = selection.getSelectedElement();

  if (selectedElement && selectedElement.is('element', 'cookiecontentblocker')) {
    return selectedElement;
  }

  return null;
}
