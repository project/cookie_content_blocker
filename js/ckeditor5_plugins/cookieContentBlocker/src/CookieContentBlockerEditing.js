import {Plugin} from 'ckeditor5/src/core';
import {toWidget} from 'ckeditor5/src/widget';

import CookieContentBlockerCommand from './CookieContentBlockerCommand';

export default class CookieContentBlockerEditing extends Plugin {
  init() {
    const editor = this.editor;

    // Get the Drupal Configurations.
    const options = editor.config.get(`cookieContentBlockerOptions`);

    this._defineSchema();
    this._defineConverters();

    editor.commands.add(
      "CookieContentBlockerCommand",
      new CookieContentBlockerCommand(editor, options)
    );
  }

  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.register('cookiecontentblocker', {
      inheritAllFrom: '$blockObject',
      allowAttributes: ['settings', 'content']
    });
  }

  _defineConverters() {
    const editor = this.editor;
    const conversion = editor.conversion;

    editor.data.registerRawContentMatcher({
      name: 'cookiecontentblocker'
    });

    // Conversion from a model to a view element
    conversion.for('editingDowncast').elementToStructure({
      model: {
        name: 'cookiecontentblocker',
        attributes: ['content']
      },
      view: (modelItem, {writer: viewWriter}) => {
        const widgetElement = viewWriter.createContainerElement('section', {
          class: 'cookie-content-blocker'
        }, [
          viewWriter.createRawElement('div', {}, function (domElement) {
            const domDocument = domElement.ownerDocument;

            // Remove all children;
            domElement.textContent = '';

            // Creating a contextual document fragment allows executing scripts when inserting into the preview element.
            // See: #8326.
            const domRange = domDocument.createRange();
            const domDocumentFragment = domRange.createContextualFragment(modelItem.getAttribute('content'));

            domElement.append(domDocumentFragment);
          })
        ]);

        // Enable widget handling on a placeholder element inside the editing view.
        return toWidget(widgetElement, viewWriter);
      }
    });

    conversion.for('dataDowncast').elementToElement({
      model: 'cookiecontentblocker',
      view: (modelElement, {writer}) => {
        return writer.createRawElement(
          'cookiecontentblocker',
          {
            class: 'cookie-content-blocker',
            'data-settings': window.btoa(modelElement.getAttribute('settings'))
          },
          function (domElement) {
            domElement.innerHTML = modelElement.getAttribute('content') || '';
          });
      }
    });

    // Conversion from a view element to a model attribute
    conversion.for('upcast').elementToElement({
      view: {
        name: 'cookiecontentblocker',
        classes: ['cookie-content-blocker'],
        attributes: ['data-settings']
      },
      model: (viewElement, conversionApi) => {
        const modelWriter = conversionApi.writer;
        const dataSettings = window.atob(viewElement.getAttribute('data-settings'));
        const content = viewElement.getCustomProperty('$rawContent');

        return modelWriter.createElement('cookiecontentblocker', {
          settings: dataSettings,
          content: content
        });
      }
    });
  }
}
