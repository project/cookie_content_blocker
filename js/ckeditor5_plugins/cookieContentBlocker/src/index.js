/**
 * @module cookie_content_blocker/CookieContentBlocker/CookieContentBlocker
 */

import {Plugin} from 'ckeditor5/src/core';

import CookieContentBlockerEditing from "./CookieContentBlockerEditing";
import CookieContentBlockerUI from "./CookieContentBlockerUI";

class CookieContentBlocker extends Plugin {
  static get pluginName() {
    return 'CookieContentBlocker';
  }

  static get requires() {
    return [CookieContentBlockerEditing, CookieContentBlockerUI];
  }
}

export default {
  CookieContentBlocker,
}
