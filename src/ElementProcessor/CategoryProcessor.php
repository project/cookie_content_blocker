<?php

namespace Drupal\cookie_content_blocker\ElementProcessor;

use Drupal\cookie_content_blocker\CookieContentBlockerCategoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Class CategoryProcessor.
 *
 * Processes category on blocked elements.
 *
 * @package Drupal\cookie_content_blocker\ElementProcessor
 */
class CategoryProcessor extends ElementProcessorBase {

  /**
   * The cookies category storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $categoryStorage;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a CategoryProcessor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer) {
    $this->categoryStorage = $entityTypeManager->getStorage('cookie_content_blocker_category');
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(array $element): bool {
    return !empty($element['#cookie_content_blocker']['category']);
  }

  /**
   * {@inheritdoc}
   */
  public function processElement(array $element): array {
    $category = $this->categoryStorage->load($element['#cookie_content_blocker']['category']);
    if (!$category instanceof CookieContentBlockerCategoryInterface) {
      // Invalid category, set to default.
      $element['#cookie_content_blocker']['category'] = '';
      return $element;
    }

    $defaults = [
      'blocked_message' => $category->get('blocked_message'),
      'show_button' => $category->get('show_button'),
      'button_text' => $category->get('button_text'),
      'enable_click' => $category->get('enable_click_consent_change'),
    ];

    $element['#cookie_content_blocker'] = array_merge($defaults, $element['#cookie_content_blocker']);

    $this->renderer->addCacheableDependency($element, $category);
    return $element;
  }
}
