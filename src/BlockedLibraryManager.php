<?php

namespace Drupal\cookie_content_blocker;

use function in_array;

/**
 * Manages libraries that are blocked until consent is given.
 *
 * @package Drupal\cookie_content_blocker
 */
class BlockedLibraryManager implements BlockedLibraryManagerInterface {

  /**
   * The list of blocked libraries.
   *
   * @var string[]
   */
  protected $blockedLibraries = [];

  /**
   * The list of categories for every library.
   *
   * A single library can be attached to elements with different categories,
   * therefor we collect all categories.
   *
   * @var array
   */
  protected $libraryCategories = [];

  /**
   * {@inheritdoc}
   */
  public function addBlockedLibrary(string $library, string $category = ''): void {
    $this->blockedLibraries[$library] = $library;
    $this->libraryCategories[$library] = $this->libraryCategories[$library] ?? [];

    if (!empty($category) && !in_array($category, $this->libraryCategories[$library], TRUE)) {
      $this->libraryCategories[$library][] = $category;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockedLibraries(): array {
    return $this->blockedLibraries;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryCategories(string $library): array {
    return $this->libraryCategories[$library] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasBlockedLibraries(): bool {
    return !empty($this->blockedLibraries);
  }

  /**
   * {@inheritdoc}
   */
  public function isBlocked(string $library): bool {
    return in_array($library, $this->blockedLibraries, TRUE);
  }

}
