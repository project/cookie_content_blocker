<?php

namespace Drupal\cookie_content_blocker;

/**
 * Interface BlockedLibraryManagerInterface.
 *
 * @package Drupal\cookie_content_blocker
 */
interface BlockedLibraryManagerInterface {

  /**
   * Add a library to the list of blocked libraries.
   *
   * @param string $library
   *   The name of the library.
   * @param string $category
   *   The (optional) cookie category.
   */
  public function addBlockedLibrary(string $library, string $category = ''): void;

  /**
   * Get a list of blocked libraries.
   *
   * @return string[]
   *   The list of blocked libraries.
   */
  public function getBlockedLibraries(): array;

  /**
   * Get all categories for a library.
   *
   * @param string $library
   *   The name of the library.
   *
   * @return array
   */
  public function getLibraryCategories(string $library): array;

  /**
   * Check whether there are any blocked libraries.
   *
   * @return bool
   *   TRUE is there is at least one blocked library, FALSE otherwise.
   */
  public function hasBlockedLibraries(): bool;

  /**
   * Check whether a single library is blocked.
   *
   * @param string $library
   *   The name of the library.
   *
   * @return bool
   *   TRUE if the library is blocked, FALSE otherwise.
   */
  public function isBlocked(string $library): bool;

}
