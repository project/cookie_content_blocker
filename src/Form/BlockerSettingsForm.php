<?php

namespace Drupal\cookie_content_blocker\Form;

use Drupal\cookie_content_blocker\Traits\CookieSettingsFormTrait;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder to manage settings related to the Cookie content blocker.
 *
 * @package Drupal\cookie_content_blocker\Form
 */
class BlockerSettingsForm extends ConfigFormBase {

  use CookieSettingsFormTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cookie_content_blocker_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cookie_content_blocker.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    return $this->doBuildCookieSettingsForm(parent::buildForm($form, $form_state), $this->config('cookie_content_blocker.settings')->get());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config('cookie_content_blocker.settings');
    $config->set('blocked_message', $form_state->getValue('blocked_message'));
    $config->set('show_button', $form_state->getValue('show_button'));
    $config->set('button_text', $form_state->getValue('button_text'));
    $config->set('enable_click_consent_change', $form_state->getValue('enable_click_consent_change'));
    $config->set('consent_awareness', $form_state->getValue('consent_awareness'));
    $config->save();
  }

}
