<?php

namespace Drupal\cookie_content_blocker\Form;

use Drupal\cookie_content_blocker\Traits\CookieSettingsFormTrait;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Cookie content blocker category form.
 *
 * @property \Drupal\cookie_content_blocker\CookieContentBlockerCategoryInterface $entity
 */
class CookieContentBlockerCategoryForm extends EntityForm {

  use CookieSettingsFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the cookie content blocker category.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cookie_content_blocker\Entity\CookieContentBlockerCategory::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the cookie content blocker category.'),
    ];

    return $this->doBuildCookieSettingsForm($form, $this->entity->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new cookie content blocker category %label.', $message_args)
      : $this->t('Updated cookie content blocker category %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
