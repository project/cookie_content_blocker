<?php

namespace Drupal\cookie_content_blocker\Entity;

use Drupal\cookie_content_blocker\CookieContentBlockerCategoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the cookie content blocker category entity type.
 *
 * @ConfigEntityType(
 *   id = "cookie_content_blocker_category",
 *   label = @Translation("Cookie category"),
 *   label_collection = @Translation("Cookie categories"),
 *   label_singular = @Translation("Cookie category"),
 *   label_plural = @Translation("Cookie categories"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Cookie category",
 *     plural = "@count Cookie categories",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\cookie_content_blocker\CookieContentBlockerCategoryListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cookie_content_blocker\Form\CookieContentBlockerCategoryForm",
 *       "edit" = "Drupal\cookie_content_blocker\Form\CookieContentBlockerCategoryForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "category",
 *   admin_permission = "administer cookie content blocker categories",
 *   links = {
 *     "collection" = "/admin/config/user-interface/cookie-content-blocker/categories",
 *     "add-form" = "/admin/config/user-interface/cookie-content-blocker/categories/add",
 *     "edit-form" = "/admin/config/user-interface/cookie-content-blocker/categories/{cookie_content_blocker_category}/edit",
 *     "delete-form" = "/admin/config/user-interface/cookie-content-blocker/categories/{cookie_content_blocker_category}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "blocked_message",
 *     "button_text",
 *     "enable_click_consent_change",
 *     "show_button",
 *     "consent_awareness"
 *   }
 * )
 */
class CookieContentBlockerCategory extends ConfigEntityBase implements CookieContentBlockerCategoryInterface {

  /**
   * The cookie content blocker category ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The cookie content blocker category label.
   *
   * @var string
   */
  protected $label;

  /**
   * The cookie_content_blocker_category description.
   *
   * @var string
   */
  protected $description;

  /**
   * The message for blocked content.
   *
   * @var string
   */
  protected $blocked_message;

  /**
   * The change cookie consent button text.
   *
   * @var string
   */
  protected $button_text;

  /**
   * Enable changing consent by clicking on the blocked content.
   *
   * @var bool
   */
  protected $enable_click_consent_change;

  /**
   * Show a button to change cookie consent.
   *
   * @var bool
   */
  protected $show_button;

  /**
   * The consent aware settings.
   *
   * @var array
   */
  protected $consent_awareness = [];

}
