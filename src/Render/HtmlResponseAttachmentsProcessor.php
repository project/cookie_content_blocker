<?php

namespace Drupal\cookie_content_blocker\Render;

use function array_diff_key;
use function array_filter;
use function array_key_exists;
use function array_keys;
use function array_map;
use function array_merge;
use function array_unshift;
use function defined;
use function in_array;
use function is_callable;
use function uasort;
use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\cookie_content_blocker\BlockedLibraryManagerInterface;
use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Asset\AssetResolver;
use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\HtmlResponseAttachmentsProcessor as CoreHtmlResponseAttachmentsProcessor;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Processes attachments of HTML responses.
 *
 * @see \Drupal\Core\Render\HtmlResponseAttachmentsProcessor
 */
class HtmlResponseAttachmentsProcessor extends CoreHtmlResponseAttachmentsProcessor {

  /**
   * The drupal settings.
   *
   * @var array
   */
  protected static $drupalSettings = [
    'type' => 'setting',
    'group' => JS_SETTING,
    'weight' => 0,
    'browsers' => [],
    'data' => [],
    'position' => 'scripts_bottom',
  ];

  /**
   * The allowed assets.
   *
   * @var \Drupal\Core\Asset\AttachedAssetsInterface
   */
  protected $allowedAssets;

  /**
   * The blocked assets.
   *
   * @var \Drupal\Core\Asset\AttachedAssetsInterface
   */
  protected $blockedAssets;

  /**
   * The library manager.
   *
   * @var \Drupal\cookie_content_blocker\BlockedLibraryManagerInterface
   */
  protected $libraryManager;

  /**
   * Constructs a HtmlResponseAttachmentsProcessor object.
   *
   * @param \Drupal\cookie_content_blocker\BlockedLibraryManagerInterface $library_manager
   *   The library manager for blocked libraries.
   * @param \Drupal\Core\Asset\AssetResolverInterface $asset_resolver
   *   An asset resolver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $css_collection_renderer
   *   The CSS asset collection renderer.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $js_collection_renderer
   *   The JS asset collection renderer.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(BlockedLibraryManagerInterface $library_manager, AssetResolverInterface $asset_resolver, ConfigFactoryInterface $config_factory, AssetCollectionRendererInterface $css_collection_renderer, AssetCollectionRendererInterface $js_collection_renderer, RequestStack $request_stack, RendererInterface $renderer, ModuleHandlerInterface $module_handler) {
    parent::__construct($asset_resolver, $config_factory, $css_collection_renderer, $js_collection_renderer, $request_stack, $renderer, $module_handler);
    $this->libraryManager = $library_manager;
    $this->blockedAssets = $this->allowedAssets = new AttachedAssets();
  }

  /**
   * Merge additional settings into the drupal settings.
   *
   * @param array $settings
   *   The settings to merge.
   */
  protected function mergeSettings(array $settings): void {
    self::$drupalSettings = NestedArray::mergeDeepArray([self::$drupalSettings, $settings], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function processAssetLibraries(AttachedAssetsInterface $assets, array $placeholders): array {
    if (!$this->libraryManager->hasBlockedLibraries()) {
      return parent::processAssetLibraries($assets, $placeholders);
    }

    $variables = [
      'styles' => [],
      'scripts' => [],
      'scripts_bottom' => [],
    ];

    $blockedLibraries = $this->libraryManager->getBlockedLibraries();
    $allowedLibraries = array_filter($assets->getLibraries(), static function ($library) use ($blockedLibraries) {
      return !in_array($library, $blockedLibraries, TRUE);
    });

    $this->allowedAssets = $assets->setLibraries($allowedLibraries);
    $this->blockedAssets = AttachedAssets::createFromRenderArray(['#attached' => ['library' => $blockedLibraries]]);
    $this->blockedAssets->setAlreadyLoadedLibraries($allowedLibraries);

    foreach (array_keys($variables) as $type) {
      $processMethod = 'process' . Container::camelize($type);
      if (!isset($placeholders[$type]) || !is_callable([$this, $processMethod])) {
        continue;
      }

      $variables[$type] = $this->{$processMethod}();
    }

    // After everything is processed we restore the drupal settings on its
    // original position, now containing our extra settings created by
    // placeholders as well.
    $settings = self::$drupalSettings;
    array_unshift($variables[$settings['position']], ...$this->renderAssetCollection([$settings], $this->jsCollectionRenderer));
    return $variables;
  }

  /**
   * Generate a placeholder script referencing to the original asset.
   *
   * @param array $asset
   *   The asset to create a placeholder for.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $assetRenderer
   *
   * @return array
   *   The placeholder render array.
   */
  private function generateAssetPlaceholder(array $asset, AssetCollectionRendererInterface $assetRenderer): array {
    $id = Html::getUniqueId(Crypt::randomBytesBase64());
    $placeholder = [
      '#type' => 'html_tag',
      '#tag' => 'script',
      '#value' => '',
      '#attributes' => [
        'data-cookie-content-blocker-asset-id' => $id,
      ],
    ];

    if (!empty($asset['categories'])) {
      $placeholder['#attributes']['data-categories'] = Json::encode($asset['categories']);
    }

    $renderedAsset = $assetRenderer->render([$asset]);
    $attached = [
      '#attached' => [
        'drupalSettings' => [
          'cookieContentBlocker' => [
            'blockedAssets' => [
              $id => (string) $this->renderer->renderPlain(...$renderedAsset),
            ],
          ],
        ],
      ],
    ];

    // Merge attached settings back into the original assets.
    $placeholderAsset = AttachedAssets::createFromRenderArray($attached);
    $this->allowedAssets->setSettings(NestedArray::mergeDeepArray([$placeholderAsset->getSettings(), $this->allowedAssets->getSettings()], TRUE));
    $this->mergeSettings(['data' => $this->allowedAssets->getSettings()]);
    return $placeholder;
  }

  /**
   * Collects the CSS assets.
   *
   * @return array
   *   The CSS asset collection.
   */
  private function getCssAssetCollection(): array {
    $optimizeCss = !defined('MAINTENANCE_MODE') && $this->config->get('css.preprocess');
    $allowedCssAssets = $this->assetResolver->getCssAssets($this->allowedAssets, $optimizeCss);
    $allowedCssAssetsRaw = $optimizeCss ? $this->assetResolver->getCssAssets($this->allowedAssets, FALSE) : $allowedCssAssets;
    $blockedCssAssets = [];

    foreach ($this->blockedAssets->getLibraries() as $blockedLibrary) {
      $categories = $this->libraryManager->getLibraryCategories($blockedLibrary);
      $blockedAsset = AttachedAssets::createFromRenderArray(['#attached' => ['library' => [$blockedLibrary]]]);
      $blockedAsset->setAlreadyLoadedLibraries($this->blockedAssets->getAlreadyLoadedLibraries());

      foreach ($this->assetResolver->getCssAssets($blockedAsset, FALSE) as $key => $resolvedAsset) {
        $resolvedAsset['categories'] = array_merge($blockedCssAssets[$key]['categories'] ?? [], $categories);
        $blockedCssAssets[$key] = $resolvedAsset;
      }
    }

    return $this->getMergedAndSortedAssets($allowedCssAssets, $allowedCssAssetsRaw, $blockedCssAssets, $optimizeCss);
  }

  /**
   * Collects the JS assets.
   *
   * @param string $region
   *   The region to retrieve assets for. Can either be 'header' or 'footer'.
   *
   * @return array
   *   The JS asset collection for the given region.
   */
  private function getJsAssetCollection(string $region): array {
    static $header = NULL, $footer = NULL, $processed = FALSE;

    if ($processed) {
      return $$region ?? [];
    }

    $optimizeJs = !defined('MAINTENANCE_MODE') && !Drupal::state()->get('system.maintenance_mode') && $this->config->get('js.preprocess');
    [$allowedJsAssetsHeader, $allowedJsAssetsFooter] = $this->assetResolver->getJsAssets($this->allowedAssets, $optimizeJs);
    [$allowedJsAssetsHeaderRaw, $allowedJsAssetsFooterRaw] = $optimizeJs ? $this->assetResolver->getJsAssets($this->allowedAssets, FALSE) : [$allowedJsAssetsHeader, $allowedJsAssetsFooter];
    $blockedJsAssetsHeader = [];
    $blockedJsAssetsFooter = [];

    foreach ($this->blockedAssets->getLibraries() as $blockedLibrary) {
      $categories = $this->libraryManager->getLibraryCategories($blockedLibrary);
      $blockedAsset = AttachedAssets::createFromRenderArray(['#attached' => ['library' => [$blockedLibrary]]]);
      $blockedAsset->setAlreadyLoadedLibraries($this->blockedAssets->getAlreadyLoadedLibraries());

      [$jsAssetsHeader, $jsAssetsFooter] = $this->assetResolver->getJsAssets($blockedAsset, FALSE);

      foreach ($jsAssetsHeader as $key => $headerAsset) {
        $headerAsset['categories'] = array_merge($blockedJsAssetsHeader[$key]['categories'] ?? [], $categories);
        $blockedJsAssetsHeader[$key] = $headerAsset;
      }

      foreach ($jsAssetsFooter as $key => $footerAsset) {
        $footerAsset['categories'] = array_merge($blockedJsAssetsFooter[$key]['categories'] ?? [], $categories);
        $blockedJsAssetsFooter[$key] = $footerAsset;
      }
    }

    // @codingStandardsIgnoreStart
    $header = $this->getMergedAndSortedAssets($allowedJsAssetsHeader, $allowedJsAssetsHeaderRaw, $blockedJsAssetsHeader, $optimizeJs);
    $footer = $this->getMergedAndSortedAssets($allowedJsAssetsFooter, $allowedJsAssetsFooterRaw, $blockedJsAssetsFooter, $optimizeJs);

    // We need to extract settings because we want to add extra settings
    // for generated placeholders, so we can identify them. Therefor we
    // render all the scripts first and attach the settings at the end, which
    // at that point will contain our placeholder settings.
    $settings = $header['drupalSettings'] ?? $footer['drupalSettings'] ?? [];
    $settings['position'] = array_key_exists('drupalSettings', $header) ? 'scripts' : 'scripts_bottom';
    $this->mergeSettings($settings);
    unset($header['drupalSettings'], $footer['drupalSettings']);

    $processed = TRUE;
    return $$region ?? [];
    // @codingStandardsIgnoreEnd
  }

  /**
   * Merges and sorts allowed and blocked assets back together.
   *
   * Marks blocked assets to be able to identify them.
   *
   * @param array $allowedAssets
   *   The allowed assets.
   * @param array $allowedAssetsRaw
   *   The raw allowed assets (un-optimized) used for comparison/diff.
   * @param array $blockedAssets
   *   The blocked assets.
   * @parem bool $optimized
   *   Whether assets are optimized, this is needed for sorting.
   *
   * @return array
   *   The merged and sorted assets,
   */
  private function getMergedAndSortedAssets(array $allowedAssets, array $allowedAssetsRaw, array $blockedAssets, bool $optimized): array {
    // Filter out assets that are allowed. This leaves us with only the
    // assets we want to block and (optional) additional dependent assets that
    // are not required by any other asset.
    $blockedAssets = $this->markAssetsAsBlocked(array_diff_key($blockedAssets, $allowedAssetsRaw));
    if ($optimized) {
      uasort($blockedAssets, [AssetResolver::class, 'sort']);
    }

    $assets = array_merge($allowedAssets, $blockedAssets);
    if (!$optimized) {
      uasort($assets, [AssetResolver::class, 'sort']);
    }

    return $assets;
  }

  /**
   * Mark all individual assets as blocked.
   *
   * Makes sure they are not preprocesses or cached.
   *
   * @param array $assets
   *   The assets to mark.
   *
   * @return array
   *   The marked assets.
   */
  private function markAssetsAsBlocked(array $assets): array {
    return array_map(static function ($asset) {
      $asset['preprocess'] = $asset['cache'] = FALSE;
      $asset['is_blocked'] = TRUE;
      return $asset;
    }, $assets);
  }

  /**
   * Processes css assets and creates output for the 'styles' variable.
   *
   * @noinspection PhpUnusedPrivateMethodInspection
   *
   * @return array
   *   The output for the 'styles' variable.
   */
  private function processStyles(): array {
    return $this->renderAssetCollection($this->getCssAssetCollection(), $this->cssCollectionRenderer);
  }

  /**
   * Processes js assets and creates output for the 'scripts' variable.
   *
   * @noinspection PhpUnusedPrivateMethodInspection
   *
   * @return array
   *   The output for the 'styles' variable.
   */
  private function processScripts(): array {
    return $this->renderAssetCollection($this->getJsAssetCollection('header'), $this->jsCollectionRenderer);
  }

  /**
   * Processes js assets and creates output for the 'scripts_bottom' variable.
   *
   * @noinspection PhpUnusedPrivateMethodInspection
   *
   * @return array
   *   The output for the 'styles' variable.
   */
  private function processScriptsBottom(): array {
    return $this->renderAssetCollection($this->getJsAssetCollection('footer'), $this->jsCollectionRenderer);
  }

  /**
   * Renders asset collections and inserts placeholders for blocked assets.
   *
   * @param array $collection
   *   The asset collection to render.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $renderer
   *   The renderer to use to render the collection.
   *
   * @return array
   *   The render array for the asset collection.
   */
  private function renderAssetCollection(array $collection, AssetCollectionRendererInterface $renderer): array {
    $rendered = [[]];
    foreach ($collection as $asset) {
      $rendered[] = !empty($asset['is_blocked']) ? [$this->generateAssetPlaceholder($asset, $renderer)] : $renderer->render([$asset]);
    }

    return array_merge(...$rendered);
  }

}
