<?php

namespace Drupal\cookie_content_blocker;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface CookieContentBlockerCategoryInterface extends ConfigEntityInterface {

}
