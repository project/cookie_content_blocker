<?php

namespace Drupal\cookie_content_blocker\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefinition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\EditorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CKEditor 5 Cookie Content Blocker plugin configuration.
 *
 * @internal
 *   Plugin classes are internal.
 */
class CookieContentBlocker extends CKEditor5PluginDefault implements ContainerFactoryPluginInterface {

  /**
   * The cookie content blocker category entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $cookieContentBlockerCategoryStorage;

  public function __construct(array $configuration, string $plugin_id, CKEditor5PluginDefinition $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cookieContentBlockerCategoryStorage = $entity_type_manager->getStorage('cookie_content_blocker_category');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $options = [];

    $categories = $this->cookieContentBlockerCategoryStorage->loadMultiple();

    foreach ($categories as $category) {
      $options['categories'][$category->id()] = $category->label();
    }

    return [
      'cookieContentBlockerOptions' => $options,
    ];
  }

}
