<?php

namespace Drupal\cookie_content_blocker\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the DraftText CKEditor plugin.
 *
 * @package Drupal\cookie_content_blocker\PLugin\CKEditorPlugin
 *
 * @CKEditorPlugin(
 *   id = "cookiecontentblocker",
 *   label = @Translation("Cookie Content Blocker"),
 *   module = "cookie_content_blocker"
 * )
 */
class CookieContentBlocker extends CKEditorPluginBase implements CKEditorPluginCssInterface, ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path of this module.
   *
   * @var string
   */
  protected $modulePath;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->modulePath = $container->get('extension.path.resolver')->getPath('module', 'cookie_content_blocker');

    return $instance;
  }


  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor): array {
    return [
      $this->modulePath . '/js/plugins/cookiecontentblocker/css/editor.css',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile(): string {
    return $this->modulePath . '/js/plugins/cookiecontentblocker/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor): array {
    $config = [
      'categories' => [],
    ];

    $categories = $this->entityTypeManager->getStorage('cookie_content_blocker_category')->loadMultiple();
    foreach ($categories as $category) {
      $config['categories'][$category->id()] = $category->label();
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons(): array {
    return [
      'CookieContentBlocker' => [
        'label' => $this->t('Add Cookie Content Blocker content'),
        'image' => $this->modulePath . '/js/plugins/cookiecontentblocker/icons/cookiecontentblocker.png',
      ],
    ];
  }

}
