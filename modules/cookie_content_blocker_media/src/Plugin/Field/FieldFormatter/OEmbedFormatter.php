<?php

namespace Drupal\cookie_content_blocker_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter as CoreOEmbedFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'oembed' formatter.
 *
 * @FieldFormatter(
 *   id = "cookie_content_blocker_oembed",
 *   label = @Translation("Cookie Content Blocker - oEmbed content"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class OEmbedFormatter extends CoreOEmbedFormatter {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): OEmbedFormatter {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = parent::viewElements($items, $langcode);
    $config = $this->configFactory->get('cookie_content_blocker_media.settings');

    if (empty($element)) {
      return $element;
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    foreach ($items as $delta => $item) {
      if (!isset($element[$delta])) {
        continue;
      }

      /** @var \Drupal\media\MediaInterface $entity */
      /** @var \Drupal\media\Plugin\media\Source\OEmbedInterface $source */
      $entity = $item->getEntity();
      $source = $entity->getSource();

      $provider = $source->getMetadata($entity, 'provider_name');
      $settings = $config->get("providers.$provider");
      if (empty($settings['blocked'])) {
        continue;
      }

      $original = $element[$delta];
      $element[$delta]['#pre_render'] = $element[$delta]['#pre_render'] ?? [];
      $element[$delta]['#pre_render'][] = 'cookie_content_blocker.element.processor:processElement';
      $element[$delta]['#cookie_content_blocker'] = [
        'blocked_message' => $settings['blocked_message']['value'],
        'original_content' => $original,
        'category' => $settings['category'] ?? '',
      ];

      if (empty($settings['show_preview'])) {
        continue;
      }

      $element[$delta]['#cookie_content_blocker']['preview'] = [
        '#theme' => 'image_style',
        '#style_name' => $settings['preview_style'] ?? 'blocked_media_teaser',
        '#uri' => $source->getMetadata($entity, 'thumbnail_uri'),
      ];
    }

    $this->renderer->addCacheableDependency($element, $config);
    return $element;

  }

}
